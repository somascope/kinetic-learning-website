<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <title>Kinetic Learning: Self-Directed Learning</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="keywords" content="Kinetic Learning, eLearning, E Learning, Distance Learning, self-directed learning, learning, kinetic, behavior-based safety, safety consultants, safety training, working smart, Kinetic Learning Solution, Guided Tour, hands-on learning, aligning business objectives, learning objectives, on-the-job, on the job, learning methodology, real-world simulations, retention, compelling content, challenge, interactive training, interactive learning">
  <meta name="description" content="Kinetic Learning develops custom self-directed learning programs that put learners in practical but risk-controlled exercises that are relevant to the learner's job. The result:  Learning that sticks. Learners become reflective, independent thinkers and problem solvers who put best practices into daily practice.">
  <link rel="stylesheet" href="css/styles.css">
  <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
</head>

<body>

  <!-- Navigation -->
  <nav class="navbar is-info" aria-label="main navigation">
    <div class="container">
      <div class="navbar-brand">
        <a href="./" class="navbar-item">
          <img src="img/kl-plbbd.png" alt="Kinetic Learning">
        </a>
        <a role="button" class="navbar-burger" id="nav-burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
        </a>
      </div>
      <div class="navbar-menu" id="nav-links">
        <div class="navbar-end">
          <a href="learning.html" class="navbar-item">Training vs. Learning</a>
          <a href="method.html" class="navbar-item">Methodology</a>
          <a href="contact.html" class="navbar-item">Contact Us</a>
        </div>
      </div>
    </div>
  </nav>

  <main class="content has-no-bottom">
    
    <!-- People learn best by doing -->
    <section class="hero is-link is-bold is-relative">
      <div class="hero-body">
        <div class="container has-text-centered">
          <h1 class="title is-2 is-spaced">People learn best by doing</h1>
          <p class="is-size-5">
            Tell me and I'll forget;<br/>
            show me and I may remember;<br/>
            <span class="has-text-weight-bold is-italic">Involve me and I'll understand.</span>
          </p>
          <p class="is-size-5 pl-6 ml-6">— Chinese proverb</p>
        </div>
      </div>
    </section>

    <!-- Intro copy -->
    <section class="hero is-link bg-img-meeting has-parallax-fixed">
      <div class="hero-body">
        <div class="container">
          <div class="columns is-centered">
            <div class="column is-6 is-size-5-desktop">
              <p>Kinetic Learning’s approach to adult learning mirrors how people learn to navigate life’s experiences and solve life’s challenges. We do that by involving learners in highly interactive simulations that challenge the learner to assess life and workplace challenges and appropriately respond.</p>
              <p>Kinetic Learning programs engage learners by emphasizing not just knowing, but also knowing <em>how</em> and knowing <em>why</em>. We call it <em>“self-directed learning”</em> because the learner is in control.</p>
              
            </div>
            <div class="column is-6 is-size-5-desktop">
              <p>By learning effective skills and behaviors through trial and error and positive reinforcement, people learn through experience, without fear of failure or real-world consequences.</p>
              <h2 class="title is-4 has-text-white">Learn more about...</h2>
              <ul>
                <li>The <a href="#cornerstones" class="has-text-primary">four cornestones</a> of our courses</li>
                <li>The difference of <a href="learning.html" class="has-text-primary"> training vs. learning</a></li>
                <li>Our instructional <a href="method.html" class="has-text-primary">methodology</a></li>
                <li><a href="#samples-toolbox" class="has-text-primary">Samples</a> of our work</li>
                <li>Tools of the trade in our <a href="#samples-toolbox" class="has-text-primary">toolbox</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="hero is-primary is-bold">
      <div class="container has-text-centered">
        <div class="hero-body" id="cornerstones">
          <div class="container">
            <p class="title">Every Kinetic Learning course is built on four cornerstones:</p>
          </div>
        </div>
      </div>
    </section>

    <!-- Four cornerstones -->
    <section class="section">
      <div class="container">
        <div class="columns is-vcentered">
          <div class="column is-5">
            <h2 class="title is-3">Align</h2>
            <p class="">Align learning objectives with the project's strategic objectives.</p>
          </div>
          <div class="column is-6 is-offset-1">
            <img src="img/home-align.jpg" class="has-shadow" alt="Eight people sitting around a table talking.">
          </div>
        </div>
      </div>
    </section>

    <section class="section">
      <div class="container">
        <div class="columns is-vcentered reverse-row-order">
          <div class="column is-5 is-offset-1">
            <h2 class="title is-3">Engage</h2>
            <p class="">Engage the learner in self-directed learning based on the simple premise that people learn best by doing.</p>
          </div>
          <div class="column is-6">
            <img src="img/home-engage.jpg" class="has-shadow" alt="A woman looking at a computer monitor.">
          </div>
        </div>
      </div>
    </section>

    <section class="section">
      <div class="container">
        <div class="columns is-vcentered">
          <div class="column is-5">
            <h2 class="title is-3">Involve</h2>
            <p class="">Involve the learner in rich and practical but risk-controlled exercises that challenge him or her to acquire and demonstrate new or improved job and problem-solving skills.</p>
          </div>
          <div class="column is-6 is-offset-1">
            <img src="img/home-involve-ui.png" class="has-shadow" alt="A screen shot of a course showing a video option being selected in a multiple choice question.">
          </div>
        </div>
      </div>
    </section>

    <section class="section">
      <div class="container">
        <div class="columns is-vcentered reverse-row-order">
          <div class="column is-5 is-offset-1">
            <h2 class="title is-3">Reinforce</h2>
            <p class="">Reinforce the knowledge and skills the learner acquires and demonstrates.</p>
          </div>
          <div class="column is-6">
            <img src="img/home-reinforce-ui.jpg" class="has-shadow" alt="A screen shot showing feedback appearing based on the user making a correct choice.">
          </div>
        </div>
      </div>
    </section>

    <!-- Samples & Toolbox -->
    <section class="hero" id="samples-toolbox">
      <div class="columns is-centered has-text-centered" style="margin: 0 0;">

        <div class="column is-6 has-background-success has-text-white p-6 is-flex-column">
          <h2 class="title is-size-3 has-text-white">Our samples</h2>
          <div class="mb-4">
            <i class="fas fa-desktop fa-6x"></i>
          </div>
          <div class="mt-4" style="max-width: 30em; margin: 0 auto;">
            <h3 class="title has-text-white">Interactive exercises and discovery activities</h3>
            <p>Experience examples of how we help learners succeed.</p>
            <a href="https://learn_by_doing.kineticlearning.com/klms3/login.html" class="button is-success is-inverted is-outlined is-medium flex-item-bottom is-button-narrow-centered mt-4" target="_blank" rel="noreferrer">
              View samples
              <span class="icon is-small ml-2">
                <i class="fas fa-external-link-alt"></i>
              </span>
            </a>
          </div>
        </div>
        
        <div class="column is-6 has-background-link has-text-white p-6 is-flex-column">
          <h2 class="title is-size-3 has-text-white">Our toolbox</h2>
          <div class="mb-4">
            <i class="fas fa-toolbox fa-6x"></i>
          </div>
          <div class="mt-4" style="max-width: 30em; margin: 0 auto;">
            <h3 class="title has-text-white">Our approach to adult learning in action</h3>
            <p>Interact with the tools of our trade, including title sequence, use of on-camera hosts, speaker support, discovery activities, knowledge checks, interactive exercises, course evaluations, and completion certificates.</p>
            <a href="http://www.kineticlearning.com/toolbox-demo/" class="button is-link is-inverted is-outlined is-medium flex-item-bottom is-button-narrow-centered mt-4" target="_blank" rel="noreferrer">
              View toolbox
              <span class="icon is-small ml-2">
                <i class="fas fa-external-link-alt"></i>
              </span>
            </a>

          </div>
        </div>

      </div>
    </section>
    
    <!-- Bottom message -->
    <section class="section">
      <div class="container">
        <div class="container">
          <div class="columns is-centered">
            <div class="column is-8">
              <p class="is-size-5">Through <strong>learn by doing</strong>, learners are transformed from passive recipients of information to actively engaged seekers of knowledge.</p>
              <p class="is-size-5"><strong>The result:</strong> reflective, independent thinkers and problem solvers who put best practices into daily practice.</p>
              <p class="has-text-weight-bold is-size-4">Learning is a journey. Begin yours here.</p>
            </div>
          </div>
        </div>
      </div>
    </section>

  </main>

  <!-- Footer -->
  <footer class="footer">
    <div class="content has-text-centered">
      <p>
        <a href="legal.html" class="has-text-white">Privacy Statement</a> | © <span class="copyrightYear">2021</span> Kinetic Learning Inc. All Rights Reserved.
      </p>
    </div>
  </footer>

  <script src="js/main.js"></script>
  <script>
    var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
    document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
  </script>
  <script>
    try {
    var pageTracker = _gat._getTracker("UA-10989317-1");
    pageTracker._trackPageview();
    } catch(err) {}
  </script>

</body>
</html>