// Menu
const navBurger = document.querySelector("#nav-burger");
const navMenu = document.querySelector("#nav-links");

navBurger.addEventListener("click", () => {
  navBurger.classList.toggle("is-active");
  navMenu.classList.toggle("is-active");
});

// Footer
const copyrightYear = document.querySelector(".copyrightYear");
copyrightYear.innerText = new Date().getFullYear();
