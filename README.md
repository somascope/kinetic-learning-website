# Kinetic Learning Website

# Project tech

This project is setup in a simple manner that doesn't require a build or bundle process if you are only making an HTML edit. If you are doing additional customizing of the Bulma CSS file (setting up brand colors, changing defaults, etc.), then there is a Node-based build process required to build the CSS using node-sass and remove unused CSS using PurgeCSS.

- [Bulma](https://bulma.io/) - A CSS framework based on Flexbox.
- [PurgeCSS](https://purgecss.com/CLI.html) - A tool to remove unused CSS, greatly reducing the file size. See the [CLI usage](https://purgecss.com/CLI.html).
- [node-sass](https://github.com/sass/node-sass) - A Node-based library that let's your build a CSS file from Sass files.

## Project repository

[https://gitlab.com/somascope/kinetic-learning-website](gitlab.com/somascope/kinetic-learning-website)

## Production/live URL:

[https://www.kineticlearning.com/](https://www.kineticlearning.com/)

## Review URL:

[https://kineticlearning.netlify.app](kineticlearning.netlify.app)

## Project install

Start by cloning the repo. To edit this project on your computer, you must also have Nove installed, and NPM (which comes with Node).

```
npm install

```

## Project development preview

Open the index.html file in your browser.

You want the Bulma file to recompile as you edit it and work on the HTML. The Sass file will be watched and recompile as you save.

```
npm run start
```

## Project build for production

Before FTP'g the files to the KL web server, prep the files for production. These will be in the dist folder

```
npm run prod
```

## Deployment (for previews only)

Deployment, handled using Netlify, is used only for previews, not for production.

1. Commit changes and push to MASTER
2. Using the Netlify repository at [https://app.netlify.com/sites/kineticlearning/settings/general](app.netlify.com/sites/kineticlearning/settings/general), an automatic deploy will begin.

## Deployment to production

- FileZilla is used to SFTP the files from a local machine to the KL production server.
- Connection to KL's VPN is required
